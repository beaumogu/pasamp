
%% On g�n�re num�riquement des X pour le calcul du SNR :
% SNRDB = zeros(1,length(param_vec));
% for cpt_param=1:n_param
% var_n = param_vec(cpt_param);
% for n = 1 : n_trial 
% % Creation de X
% prout
% s     = zeros(dim_s,1);                 % support
% x     = zeros(dim_s,1); 
% s(:) = 0;
% x(:) = 0;
% idx_perm = randperm(dim_s);
% idx_nz   = idx_perm(1:k);
% 
% s(idx_nz)    =              1;
% x(idx_nz)    =              mean_x+sqrt(var_x)*randn(k,1)+1i*sqrt(var_x)*randn(k,1);
% n         =              sqrt(0.5*var_n)*randn(dim_y,1)+1i*sqrt(0.5*var_n)*randn(dim_y,1);
% [m_phase,icov_phase] =      paramPhaseRandom(dim_y, phase_opt);
% [p,~,~]      =              genPhaseRandom(dim_y,m_phase,icov_phase);
% signalutile=              exp(1i*p).*(D*x);
% 
% SNRDB(1,cpt_param) = SNRDB(1,cpt_param) + 10*log10(norm(signalutile)^2/norm(n)^2);
% end
% vectSNR = SNRDB./n_trial;
% end
var_tmp = perfxAddxPhase';
variances=param_vec;
var10=variances;
pseudoinverse=var_tmp(:,1);
VBEM=var_tmp(:,3);
PASAMP=var_tmp(:,4);
PRSAMP=var_tmp(:,5);

figure 
semilogx(var10,PASAMP,'b-s','LineWidth',1.3)
hold on 
semilogx(var10,VBEM,'r--o','LineWidth',1.3)
hold on 
semilogx(var10,pseudoinverse,'k--v','LineWidth',1.3)
hold on 
semilogx(var10,PRSAMP,'k-d','LineWidth',1.3)
grid on
xlabel('\sigma ^2' )
ylabel('Normalized correlation')
%set(gca,'xlim',[-25 0])

legend('Location','best','paSAMP','paVBEM','Formation de voies','prSAMP')

fig = gcf;
fig.PaperPositionMode = 'auto'
fig_pos = fig.PaperPosition;
fig.PaperSize = [fig_pos(3) fig_pos(4)];

print(fig,'k10','-dpdf')
%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% fonctions annexes 


function [p,U,Lambda] = genPhaseRandom(dim_y, m, icov)
% Model:

p=zeros(dim_y,1);

[U,Lambda]=svd(icov);

p(:) = m + U*((1./sqrt(diag(Lambda))).*randn(dim_y,1));


end



function [m,icov] = paramPhaseRandom(dim_y,phase_opt)

m         = zeros(dim_y,1);
var_pt    = phase_opt.var_pt;
var_p1    = phase_opt.var_p1; 
drift     = phase_opt.drift;
flag_covp = phase_opt.flag_covp;

if strcmp(flag_covp,'full')
    one_vec       = ones(dim_y,1)/sqrt(dim_y);
    mattmp        = randn(dim_y,dim_y-1);
    mattmp(:)     = orth(mattmp-one_vec*one_vec'*mattmp);
    U             = [one_vec mattmp];
    
    Lambda        = rand(dim_y,1);
    min_Lambda    = min(Lambda);
    Lambda(:)     = Lambda/min_Lambda*(1./var_pt);
    Lambda(1)     = 1./var_p1;   % inverse variance associated to direction 'one_vec'
    
    %[U,Lambda]=svd(mattmp);
    %U(:,1)=1/sqrt(dim_y);
    
    icov       = U*diag(Lambda)*U';
elseif strcmp(flag_covp,'Markov')
    mattmp        = zeros(dim_y,3);
    mattmp(:,1)   = -drift/var_pt;
    mattmp(:,3)   = -drift/var_pt;
    mattmp(:,2)   = (1+drift^2)/var_pt;
    mattmp(1,2)   = drift^2/var_pt+1/var_p1;
    mattmp(end,2) = 1/var_pt;
    icov=full(spdiags(mattmp,[-1 0 1],dim_y,dim_y));
    %diag(inv(icov))'
    %pause
elseif strcmp(flag_covp,'MarkovRand')
    mattmp        = zeros(dim_y,3);
    vartmp        = var_pt*rand(dim_y,1);
    mattmp(:,1)   = -drift./vartmp;
    mattmp(:,3)   = -drift./vartmp;
    mattmp(:,2)   = (1+drift^2)./vartmp;
    mattmp(1,2)   = drift^2/vartmp(1)+1/var_p1;
    mattmp(end,2) = 1/vartmp(end);
    icov=full(spdiags(mattmp,[-1 0 1],dim_y,dim_y));    
elseif strcmp(flag_covp,'iid')    
    icov = eye(dim_y)/var_p1;
elseif strcmp(flag_covp,'1DVM')
    icov = diag(var_pt*ones(dim_y,1));    
else
    error('Invalid choice for flag_covp')
end

% [Lambda]=svd(icov);
% min_Lambda= min(Lambda)
% pause
end

