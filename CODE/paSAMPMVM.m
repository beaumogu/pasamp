function [a] = paSAMP(Y,H,opt,p,phase_opt)

[M, N] = size(H);

format long 

%% Get parameters
rho = opt.rho;
delta = opt.delta;
display = opt.display;
converg = opt.converg;
damp = opt.damp;
amptype = opt.amptype;
a = opt.init_a;
c = opt.init_c;
vnf = opt.vnf;
niter = opt.niter;
meanremov = opt.meanremov;
adaptdelta = opt.adaptdelta;
adapttheta=opt.adapttheta;
mean_0_p = phase_opt.mean_p;
ivar_0_p = phase_opt.icov_theta;

%% Initialization
g = 0;
S = zeros(N, 1);
R = zeros(N, 1);
%aoldvect=zeros(N,1);
%% main loop
absH2 = abs(H).^2;
conjH = conj(H);

for i =1:niter
    %i;
    V =double(absH2*single(c));
    O = double(H*a - V.*g);
    
    [g, dg] = goutPa(Y,O,V,delta,vnf,meanremov,opt,p,phase_opt);
    
    if(strcmp(amptype, 'SwAMP'))
        g_old = g;
        %dg_old = dg;
        ind = randperm(N,N);
        for j=1:N
            k = ind(j);
            
            if(i>1)
                S(k) = damping(1/(sum(absH2(:,k).*(-dg))), S(k), damp);
            else
                S(k) = 1/(sum(absH2(:,k).*(-dg)));
            end
            
%             if S(k)<0
%                 S(k) = vnf;
%             end
            
            if(i>1)
                R(k) = damping(a(k)+S(k)*sum(conjH(:,k).* g), R(k), damp);
            else
                R(k) = a(k)+S(k)*sum(conjH(:,k).* g);
            end
            a_old = a(k);
            c_old = c(k);
            
            
            [a(k), c(k)] = BernoulligaussianPrior(S(k),R(k),rho,opt.xm,opt.xv,vnf,a_old);
            if ~isfinite(a(k))
                a(k)=a_old;
                c(k)=c_old;
            end
            
            if(meanremov && k>N-2)
                a(k) = R(k);
                c(k) = S(k);
            end
            
            VOld = V;
            
            V = V + absH2(:,k)*(c(k)-c_old);
            
            O = O + H(:,k)*(a(k)-a_old) - g_old .* (V-VOld);
            
            
            [g, dg] = goutPa(Y,O,V,delta,vnf,meanremov,opt,p,phase_opt);
        end

        
    end
    Z_est=H*a;
    Z_est_var=c'*diag(H'*H);
    %% EM step for muX noise estimation phase_opt
    
    
    if adapttheta % Ne pas utiliser !!!!
        zest = H*a;
        t    = conj(Y).*zest;
        
        icov_y = diag(sparse(2*abs(t)/delta));        
        icov_p = icov_y+phase_opt.icov_theta;
        
        var_p = diag(inv(icov_p));
        moy_p = icov_p\(-icov_y*atan2(imag(t),real(t))+ivar_0_p*mean_0_p); %
        
        %% Maintenant que les nouveau parametres de p(theta) sont estimes,  on reecrit le prior
        phase_opt.mean_p  = moy_p;
        phase_opt.varmarg = var_p;
    end
    
    if adaptdelta
        delta = noiselearn(Y,Z_est,Z_est_var,phase_opt,M);
    end   
    
end
end




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [g, dg] = goutPa(Y,O,V,delta,vnf,meanremov,opt,p,phase_opt) % CHECKED
% author : Guigui
% Calculation of the "outgoing" messages
% Voir à implémenter dans la génération du bruit les parametres:
% Kappa ( vecteur taille n )
% Lambda ( matrice de cov nxn) : rappel , diagonale nulle

format long 



if(meanremov)
    D =  delta;
    D(end-1:end) = 0;
else 
    D =  delta;
end


alpha1=(2.*abs(Y).*abs(O))./(V+D);
alpha3 = 2.*Lambda*(2.*sin(phase_opt.mean_p));
Abob = abs(alpha1.*exp(-1i.*atan2(imag(conj(Y).*O),real(conj(Y).*O)))+(Kappa-alpha3).*exp(1i.*phase_opt.mean_p));
thetabob = angle(alpha1.*exp(-1i.*atan2(imag(conj(Y).*O),real(conj(Y).*O)))+(Kappa-alpha3).*exp(1i.*phase_opt.mean_p));


% 
% thetam2=((-atan2(imag(conj(Y).*O),real(conj(Y).*O))./a)+(phase_opt.mean_p./phase_opt.varmarg))./((1./a)+(1./phase_opt.varmarg));
% 
% SigmaT2=1./((1./a)+(1./phase_opt.varmarg));

E_zIy=((Y.*V)./(D+V)).*exp(-1i*thetabob).*(R0(Abob)) + ((O.*D)./(V+D));

%var_zIy=(((abs(Y.*V.*exp(-1i*thetam2)+O.*D))./(abs(D + V))).^2).*R0(1./SigmaT2)+((V.*D)./(V+D))-abs(E_zIy).^2;
var_zIy= (1./(abs(D + V))).*(abs(Y.*V).^2+abs(O.*D).^2+2.*(abs(Y.*V).*abs(O.*D).*(R0(Abob)).*cos(thetabob+atan2(imag(conj(Y).*O),real(conj(Y).*O)))))+((V.*D)./(V+D))-abs(E_zIy).^2;
% var_zIy(var_zIy<0)=vnf;

g= (1./V).*(E_zIy-O);
dg=(1./V).*((var_zIy./V) -1);

end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function res = damping(newVal,oldVal,damp)
res=(1-damp).*newVal+damp.*oldVal;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [a, c] = BernoulligaussianPrior(S,R,rho,xm,xv,vnf,~)
% integration d'un prior Bernouilli gaussien au vecteur X.
SIGMA2=((xv.*S)./abs(xv+S));
MOYENNE=((xv.*R+S.*xm)./abs(S+xv));
% Calcul de la constante de normalisation du prior
Znor=rho.*sqrt(2*pi*SIGMA2).*exp((-abs(xm-R)^2)./(2.*abs(S+xv)))+(1-rho).*exp((-abs(R)^2)./abs(2.*S));
Znor(Znor<eps) = vnf;
% Calcul des moyennes et variances du nouveau prior
a=(1./Znor).*rho.*exp((-abs(xm-R)^2)./(2.*abs(S+xv))).*sqrt(2*pi*SIGMA2).*MOYENNE;
c=(1./Znor).*rho.*exp((-abs(xm-R)^2)./(2.*abs(S+xv))).*sqrt(2*pi*SIGMA2).*(abs(MOYENNE).^2+SIGMA2) - abs(a).^2;
c(c<eps) = vnf;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [var_n] = noiselearn(Y,a,c,phase_opt,dim_y)
ybar= Y.*exp(-1i*phase_opt.mean_p).*R0(full(1./phase_opt.varmarg));
var_n=(1./(dim_y)).*(Y'*Y-2*real(ybar'*a)+a'*a+c);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function result = R0(phi)
num = besseli(1,phi,1);
 denom = besseli(0,phi,1);
result = num./denom;  
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


