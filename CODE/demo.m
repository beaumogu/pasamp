clear all
%% Fichier test de l'algorithme Pa-SAMP
N = 64;                                                                    % Nombre de composantes du vecteur X
M = 256;                                                                   % Nombre de mesures 
K=2;                                                                       % Nombre de composantes actives (parcimonie)
rho = K/N;                                                                 % Taux de Parcimonie
varnoise = 1e-2;                                                           % Variance du bruit additif
Zech= 3000;                                                                % Nombre de r�alisation pour Theta / MONTE CARLO
wavelength = 4;                                                            % Longueur d'onde normalis�e (>2)
sig=sqrt(varnoise);                                                        % Calcul de l'�cart type du bruit additif
xm=1;
xv=1;


%% Cr�ation du vecteur Signal X
X = xm + sqrt (xv)*(randn(N,1));
ind = randperm(N,N-K);
X(ind) = 0;
%% Cr�ation de la mesure avec bruit multiplicatif
phase_opt.var_p1     = 10^6;    % variance of the initial phase
phase_opt.var_pt     = 10^-0;   % variance of transition
phase_opt.drift      = 0.8;     % multiplicative factor

[m,icov] = paramPhaseRandom(M,phase_opt);
[p,U,Lambda] = genPhaseRandom(M, m, icov);
P_noise=exp(1i*p).*eye(M);

%% Cr�ation des r�aisation de THETA pour l'integration de monte carlo
THETA = m + U*((1./sqrt(diag(Lambda))).*randn(M,Zech));
%
%% Construction de la matrice de projection dans l'espace des mesures
D=dicoDoa(M,N,wavelength);
% H = complex(normrnd(0, sqrt(1/N), M, N), normrnd(0, sqrt(1/N), M, N));
%% Bruit additif
noise = complex(normrnd(0, sig, M, 1), normrnd(0, sig, M, 1));
%% Construction du vecteur des mesures
Y = P_noise*D*double(X)+noise;
%% Options de l'algorithme pa-SAMP
opt_SAMP.xm=xm;
opt_SAMP.xv=xv;
opt_SAMP.var_mar=var(THETA);                                               % Variance du bruit de phase                                            
opt_SAMP.meantheta=mean(THETA);                                            % Moyenne du bruit de phase
opt_SAMP.damp = 0.9;                                                       % Facteur de Damping ( optimal � 0.9 / va savoir ) 
opt_SAMP.learning = 0;                                                     % Apprentisage de QUELQUE CHOSE
opt_SAMP.rho = rho;                                                        % Taux de parcimonie
opt_SAMP.delta=varnoise;                                                   % Variance du Bruit Additif
opt_SAMP.display=0;                                                        % Affichage de l'�volution de l'Algo
opt_SAMP.converg=1e-5;                                                     % Seuil de convergence de l'algorithme
opt_SAMP.niter=20;                                                         % Nombre d'it�ration du Message passing
opt_SAMP.amptype = 'SwAMP';                                                % Type d'algo / 'SwAMP'
opt_SAMP.init_a = xm.*ones(N,1);                                           % Initialisation de la moyenne de Xhat
opt_SAMP.init_c = xv*ones(N,1);                                            % Initialisation de la variance de Xhat
opt_SAMP.vnf = 0.5;                                                        % Facteur de correction de variance 
opt_SAMP.meanremov = 0;                                                    % Mean removal
opt_SAMP.adaptdelta = 1;                                                   % Adaptative step
opt_SAMP.signal = X;                                                       % Variable de stockage de la v�rit�

%% Execution de pa-SAMP
x_hat = prSAMP(Y,P_noise*D,opt_SAMP,THETA); 
%% Calcul de performances
corr = double(abs(x_hat'*X/(norm(x_hat)*norm(X))))
