function S = MUSIC(Y,A,P)
% Implémentation de MUSIC par Angélique Dremeau (angelique.dremeau@ensta-bretagne.fr) 
%----------------------------------------------
% Inputs
% - Y: observations (matrix or vector)
% - A: dictionary of steering vectors
% - P: number of eigen values considered
%----------------------------------------------
% Outputs
% - S: MUSIC power spectrum

[N,M] = size(Y);

% Correlation matrix
% Calcul de la matrice d'autocorrélation des mesures 
if M == 1
    R = Y*Y';
elseif N == 1
    R = Y'*Y;
else
    R = zeros(N);
    for l = 1:M
        R = R + Y(:,l)*Y(:,l)';
    end
    R = R/M;
end
% Calcul des valeurs propres de la matrice	
[V,D] = eig(R);
% Tri des valeurs propre
[~, idx_sort] = sort(diag(D),'descend');
% les N-P valeurs propres représentent alors le bruit , les P plus grandes vp le signal 
V_noise  = V(:,idx_sort(P+1:end));

S = diag(A'*(V_noise*V_noise')*A);
S = 1./S - diag(A'*A);



