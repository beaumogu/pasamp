function [perfxAddxPhase]=main_TESTBENCH
clear all 
close all
clc
%% Generative model parameters
%warning('off')

% mise a l'epreuve du paSAMP pour etudier ses performances en model match
% avec variation du bruit additif et multiplicatif (phase). 


%format long 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Definition des parametres 
vect_N = [10 20 50 100 150 200 256];
for dim_s = vect_N                      % Number of potential DOA in the dictionnary
vect_k = [1 2 5 10 20 25 50 100 200]; 
vect_k = vect_k(vect_k<dim_s);
for k = vect_k
clearvars -except k vect_k dim_s             % Make sure that each case is solved independently from the other
dim_y      = 256;                       % Number of sensors in the antenna
%dim_s      = 50;                       % Number of DOA in the dictionnary
vnfvalue = eps;                         % Safety value to avoid Nan

n_trial    = 100 ;                      % number of trials for each algorithm
var_x      = 10^1;                      % variance of the non-zero coefficients
mean_x     = 0 + 1i.*0;                 % mean value of the non-zero coefficients 

var_p1     = 10^1 ;                     % variance of the initial phase
drift      = 1;                         % multiplicative factor
wavelength = 4;                         % Normalised wavelength (> 2)
flag_dico  = 'DoA';                     % 'RandMod1', 'DoA'
flag_covp  = 'Markov';                  % 'iid','Markov','MarkovRand','full'

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Definition des variables 

% dico 
dico_opt.wavelength = wavelength;
dico_opt.flag_dico  = flag_dico;

% param phase model
phase_opt.meantheta  = 0;
phase_opt.var_p1     = var_p1;
phase_opt.drift      = drift;
phase_opt.flag_covp  = flag_covp;

% param algo
algo_opt.var_a      = var_x;
algo_opt.var_n      = 0;

algo_opt.flag_est_n = 'off';
algo_opt.niter      = 500;
algo_opt.pas_est    = 0.1;
algo_opt.flag_cv    = []; %'KL'

% On défini les conditions de bruit: 
param_name = 'additive noise_variance';
param_vec  = 10.^(-2:0.5:1)';
n_param    = length(param_vec);

param_name2 = 'phase noise_variance';
param_vec2  = 10.^(-2:0.25:0)';
%param_vec2  = 10.^(-1)';
n_param2    = length(param_vec2);


n_algo     = 5;                     % CBF , MUSIC , paVBEM , paSAMP, prSAMP

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


D     = dicoGen(dim_y,dim_s,dico_opt);
s     = zeros(dim_s,1);             % support
x     = zeros(dim_s,1);             % nonzero coef
x_hat1 = zeros(dim_s,1);            % estimates of x (one per algorithm)
x_hat2 = zeros(dim_s,1);
x_hat3 = zeros(dim_s,1); 
x_hat4 = zeros(dim_s,1); 
x_hat5 = zeros(dim_s,1); 
y     = zeros(dim_y,1);             % 
n     = zeros(dim_y,1);             % noise

% Ouput tensor
perfxAddxPhase = zeros(n_algo,n_param,n_param2);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for cpt_param2=1:n_param2
    fprintf([param_name2 ' = %3.4f\n'],param_vec2(cpt_param2));
    phase_opt.var_pt = param_vec2(cpt_param2);

    for cpt_param=1:n_param
        fprintf([param_name ' = %3.4f\n'],param_vec(cpt_param));
        var_n = param_vec(cpt_param);

        for cpt_trial=1:n_trial
            % initialisations.
            s(:) = 0;
            x(:) = 0;
            % Creation de X
            idx_perm = randperm(dim_s);
            idx_nz   = idx_perm(1:k);

            s(idx_nz)    =              1;
            x(idx_nz)    =              mean_x+sqrt(var_x)*randn(k,1)+1i*sqrt(var_x)*randn(k,1);
            n(:)         =              sqrt(0.5*var_n)*randn(dim_y,1)+1i*sqrt(0.5*var_n)*randn(dim_y,1);
            [m_phase,icov_phase] =      paramPhaseRandom(dim_y, phase_opt);
            [p,~,~]      =              genPhaseRandom(dim_y,m_phase,icov_phase);
            y(:)         =              exp(1i*p).*(D*x)+n;
            phase_opt.icov_theta =      icov_phase;
            phase_opt.mean_p=           m_phase;
            phase_opt.varmarg=          diag(inv(phase_opt.icov_theta));

            % LS -> conventional BF
            %%%%%%%%%%%%%%%%%%%%%%%
            x_hat1(:)  = transpose(D)*y./diag(transpose(D)*D);
            fprintf('pseudo-inverse  = %1.3f\n',abs(x_hat1'*x/(norm(x_hat1)*norm(x))));
            perfxAddxPhase(1,cpt_param,cpt_param2)=perfxAddxPhase(1,cpt_param,cpt_param2) + abs(x_hat1'*x/(norm(x_hat1)*norm(x))); % NormCorr ( metrique de performances de reconstruction de vecteur       

            % MUSIC 
            %%%%%%%%%%%%%%%%%%%%%%%
            x_hat2(:) = MUSIC(y,D,k);
            fprintf('MUSIC  = %1.3f\n',abs(x_hat2'*x/(norm(x_hat2)*norm(x))));
            perfxAddxPhase(2,cpt_param,cpt_param2)=perfxAddxPhase(2,cpt_param,cpt_param2) + abs(x_hat2'*x/(norm(x_hat2)*norm(x)));

            % paVBEM (BG prior on sources, Markov model on phases)
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            initbayes = 1*randn(dim_s,1)+1*1i*randn(dim_s,1);
            
            % Initialisations
            sparserate= double(k/dim_s);
            algo_opt.x0          = initbayes;
            algo_opt.ps          = sparserate*ones(dim_s,1);
            algo_opt.moy_theta   = m_phase;
            algo_opt.icov_theta  = icov_phase;
            algo_opt.flag_est_var_theta    = 'off';
            algo_opt.var_n       = var_n;
            % Execution de paVBEM
            [x_hat3(:),~]         = prVBEM_general_BG(y,D,algo_opt);
            fprintf('paVBEM Markov BG  = %1.3f\n',abs(x_hat3'*x/(norm(x_hat3)*norm(x))))
            perfxAddxPhase(3,cpt_param,cpt_param2)   = perfxAddxPhase(3,cpt_param,cpt_param2)+abs(x_hat3'*x/(norm(x_hat3)*norm(x)));
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % prSAMP 
            opt_pSAMP.xm        = mean_x;
            opt_pSAMP.xv        = var_x;
            opt_pSAMP.damp      = 0.9;                                     % Facteur de Damping ( optimal a 0.9 -empirical experiments-) 
            opt_pSAMP.learning  = 0;                                       % Relicat du PrSAMP
            opt_pSAMP.rho       = k/dim_s;                                 % Taux de parcimonie
            opt_pSAMP.delta     = var_n;                                   % Variance du Bruit Additif
            opt_pSAMP.display   = 0;                                       % Affichage de l'�volution de l'Algo
            opt_pSAMP.converg   = 1e-6;                                    % Seuil de convergence de l'algorithme
            opt_pSAMP.niter     = algo_opt.niter;                          % Nombre d'it�ration du Message passing
            opt_pSAMP.amptype   = 'SwAMP';                                 % Type d'algo / 'SwAMP'
            opt_pSAMP.init_a    = initbayes;                               % Initialisation de la moyenne de Xhat
            opt_pSAMP.init_c    = 100*ones(dim_s,1);                       % Initialisation de la variance de Xhat
            opt_pSAMP.vnf       = 0.5;                                     % Facteur de correction de variance 
            opt_pSAMP.meanremov = 0;                                       % Mean Removal ( not used here )
            opt_pSAMP.adapttheta= 1;                                       % EM step for phase structure Estimation
            opt_pSAMP.adaptdelta = 1;                                      % Adaptative step
            opt_pSAMP.signal     = x;
            
            % Execution de l'algorithme
            
            x_hat5(:) = prSAMP(abs(y),D,opt_pSAMP);
            fprintf('prSAMP  = %1.3f\n',abs(x_hat5'*x/(norm(x_hat5)*norm(x))));
            perfxAddxPhase(5,cpt_param,cpt_param2)  = perfxAddxPhase(5,cpt_param,cpt_param2)+abs(x_hat5'*x/(norm(x_hat5)*norm(x)));

            % paSAMP (BG prior on sources, Gaussian iid prior on phases)
            opt_SAMP.xm        = mean_x;
            opt_SAMP.xv        = var_x;
            opt_SAMP.damp      = 0.9;                                      % Facteur de Damping ( optimal à 0.9 / va savoir ) 
            opt_SAMP.learning  = 0;                                        % Apprentisage de QUELQUE CHOSE
            opt_SAMP.rho       = sparserate;                               % Taux de parcimonie
            opt_SAMP.delta     = var_n;                                    % Variance du Bruit Additif
            opt_SAMP.display   = 0;                                        % Affichage de l'évolution de l'Algo
            opt_SAMP.converg   = 1e-6;                                     % Seuil de convergence de l'algorithme
            opt_SAMP.niter     = algo_opt.niter;                           % Nombre d'itération du Message passing
            opt_SAMP.amptype   = 'SwAMP';                                  % Type d'algo / 'SwAMP'
            opt_SAMP.init_a    = initbayes; %x_hat3;%                      % Initialisation de la moyenne de Xhat
            opt_SAMP.init_c    = 100*ones(dim_s,1);                        % Initialisation de la variance de Xhat
            opt_SAMP.vnf       = vnfvalue;                                 % Facteur de correction de variance 
            opt_SAMP.meanremov = 0;
            opt_SAMP.adapttheta= 0;                                        
            opt_SAMP.adaptdelta = 1;                                       % Adaptative step on additive noise ON
            opt_SAMP.signal     = x;
            
            % Execution de l'algorithme 
            
            x_hat4(:) = paSAMP(y,D,opt_SAMP,p,phase_opt);
            fprintf('paSAMP Markov BG  = %1.3f\n',abs(x_hat4'*x/(norm(x_hat4)*norm(x))));
            perfxAddxPhase(4,cpt_param,cpt_param2)  = perfxAddxPhase(4,cpt_param,cpt_param2)+abs(x_hat4'*x/(norm(x_hat4)*norm(x)));
            disp(' ')
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        end
    end
    

end

perfxAddxPhase=perfxAddxPhase./n_trial;                                    % Moyennage

save_filename=['Results/Corr_PR_' param_name '_dimy' num2str(dim_y) '_dims' num2str(dim_s) ...
                '_k' num2str(k) '_ntrial']

save([save_filename  '.dat'],'perfxAddxPhase','-ascii')
save([save_filename  '.mat'])


%save('ResultatsDef128_64_2sourcesTEST50IT.mat','perfxAddxPhase');
end
end
end


function D=dicoGen(dim_y,dim_s,dico_opt)

flag_dico = dico_opt.flag_dico;

if strcmp(flag_dico,'RandMod1')
    D=randn(dim_y,dim_s)+ 1i * randn(dim_y,dim_s);
    D(:)=D./abs(D);
elseif strcmp(flag_dico,'DoA')
    lambda=dico_opt.wavelength;
    %doa=(0:1:dim_s-1)'*(pi/2)/dim_s;
    doa=(-pi/2)+(0:1:dim_s-1)'*(pi)/dim_s;      % between -pi/2 and pi/2
    D=exp(1i*2*pi*(1:1:dim_y)'*sin(doa')/lambda);
else
    error('Invalid choice for flag_dico')
end

% Normalisation de la sensing matrix

normD=sqrt(sum(abs(D).^2,1));
D(:)=D./repmat(normD,dim_y,1);
end



function [p,U,Lambda] = genPhaseRandom(dim_y, m, icov)
% Model:

p=zeros(dim_y,1);

[U,Lambda]=svd(icov);

p(:) = m + U*((1./sqrt(diag(Lambda))).*randn(dim_y,1));


end



function [m,icov] = paramPhaseRandom(dim_y,phase_opt)

m         = zeros(dim_y,1);
var_pt    = phase_opt.var_pt;
var_p1    = phase_opt.var_p1; 
drift     = phase_opt.drift;
flag_covp = phase_opt.flag_covp;

if strcmp(flag_covp,'full')
    one_vec       = ones(dim_y,1)/sqrt(dim_y);
    mattmp        = randn(dim_y,dim_y-1);
    mattmp(:)     = orth(mattmp-one_vec*one_vec'*mattmp);
    U             = [one_vec mattmp];
    
    Lambda        = rand(dim_y,1);
    min_Lambda    = min(Lambda);
    Lambda(:)     = Lambda/min_Lambda*(1./var_pt);
    Lambda(1)     = 1./var_p1;
    icov          = U*diag(Lambda)*U';
elseif strcmp(flag_covp,'Markov')
    mattmp        = zeros(dim_y,3);
    mattmp(:,1)   = -drift/var_pt;
    mattmp(:,3)   = -drift/var_pt;
    mattmp(:,2)   = (1+drift^2)/var_pt;
    mattmp(1,2)   = drift^2/var_pt+1/var_p1;
    mattmp(end,2) = 1/var_pt;
    icov=full(spdiags(mattmp,[-1 0 1],dim_y,dim_y));

elseif strcmp(flag_covp,'MarkovRand')
    mattmp        = zeros(dim_y,3);
    vartmp        = var_pt*rand(dim_y,1);
    mattmp(:,1)   = -drift./vartmp;
    mattmp(:,3)   = -drift./vartmp;
    mattmp(:,2)   = (1+drift^2)./vartmp;
    mattmp(1,2)   = drift^2/vartmp(1)+1/var_p1;
    mattmp(end,2) = 1/vartmp(end);
    icov          = full(spdiags(mattmp,[-1 0 1],dim_y,dim_y));    
elseif strcmp(flag_covp,'iid')    
    icov          = eye(dim_y)/var_p1;
elseif strcmp(flag_covp,'1DVM')
    icov          = diag(var_pt*ones(dim_y,1));    
else
    error('Invalid choice for flag_covp')
end

end

